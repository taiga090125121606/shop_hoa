<div id="bsm_about_us" class="container-fluid hidden-xs">
    <div class="bsm_about_us_left_side">
        <img src="public/img/about/bsm_left_side1.png" alt="Giới thiệu">
    </div>
    <div class="bsm_about_us_center">
        <div class="bsm_about_us_center_content">
            <div class="col-md-12 col-sm-12 col-xs-12 bsm_title_section">
                <h1 style="color: black">Giới thiệu</h1>
                <p>Cam kết cung cấp cho quý khach dịch vụ tốt nhất, giao hàng nhanh nhất với những đóa hoa chất lượng nhất.</p>
            </div>
        </div>
    </div>
    <div class="bsm_about_us_right_side">
        <img src="public/img/about/bsm_right_side2.png" alt="Giới thiệu">
    </div>
</div>
