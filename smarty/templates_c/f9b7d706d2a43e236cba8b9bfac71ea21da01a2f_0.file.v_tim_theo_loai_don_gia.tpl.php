<?php
/* Smarty version 3.1.30, created on 2021-08-11 09:54:03
  from "D:\xampp\htdocs\shop_hoa\views\hoa\v_tim_theo_loai_don_gia.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_6113821bcf87a3_73696334',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'f9b7d706d2a43e236cba8b9bfac71ea21da01a2f' => 
    array (
      0 => 'D:\\xampp\\htdocs\\shop_hoa\\views\\hoa\\v_tim_theo_loai_don_gia.tpl',
      1 => 1628668442,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6113821bcf87a3_73696334 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div class="col-left sidebar col-lg-3 col-md-3 col-sm-3 col-xs-12 left-color color f-left">
    <div class="anav-container">
        <div class="block block-anav">
            <ul class="magictabs">
                <li class="item active loaded single">
                    <span class="title">Tìm theo loại sản phẩm</span>
                </li>
            </ul>
            <ul class="nav-accordion left">
                <ul class="level0" style="display: block;">
                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['doc_loai_hoa']->value, 'loai_hoa');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['loai_hoa']->value) {
?>
                    <li>
                        <span class="maLoai" id="maloai_<?php echo $_smarty_tpl->tpl_vars['loai_hoa']->value->MaLoai;?>
"><?php echo $_smarty_tpl->tpl_vars['loai_hoa']->value->TenLoai;?>
</span>
                    </li>
                    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

                </ul>
            </ul>
        </div>
        <div class="block block-layered-nav block-layered-nav--no-filters">
            <ul class="magictabs">
                <li class="item active loaded single">
                    <span class="title">Tìm theo giá</span>
                </li>
            </ul>
            <div class="block-content toggle-content1">
                <div id="narrow-by-list">
                    <dl class="narrow-by">
                        <dl class="narrow-by">
                            <dt class="odd">Giá bán</dt>
                            <dt class="toggle-tab mobile even" style="display:none"></dt>
                            <dd class="Giá bán toggle-content odd">
                                <ol>
                                     <li class="tim_theo_gia" id="0-5000000">
                                        <span class="price">0&nbsp;₫ - </span>
                                        <span class="price"><?php echo number_format(5000000);?>
&nbsp;₫</span>
                                     </li>
                                     <li class="tim_theo_gia" id="5000000-11000000">
                                        <span class="price"><?php echo number_format(5000000);?>
&nbsp;₫ - </span>
                                        <span class="price"><?php echo number_format(11000000);?>
&nbsp;₫</span>
                                     </li>
                                 </ol>
                            </dd>
                    </dl>
                </div>
            </div>
        </div>
    </div>
</div>
<?php }
}
