<?php
/* Smarty version 3.1.30, created on 2021-08-10 21:31:29
  from "D:\xampp\htdocs\shop_hoa\views\v_gioi_thieu.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_6112d41157ac80_30152354',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'b923c75de0a3d7f9da983e59fd6bac42446e5b9c' => 
    array (
      0 => 'D:\\xampp\\htdocs\\shop_hoa\\views\\v_gioi_thieu.tpl',
      1 => 1628623888,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_6112d41157ac80_30152354 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div id="bsm_about_us" class="container-fluid hidden-xs">
    <div class="bsm_about_us_left_side">
        <img src="public/img/about/bsm_left_side1.png" alt="Giới thiệu">
    </div>
    <div class="bsm_about_us_center">
        <div class="bsm_about_us_center_content">
            <div class="col-md-12 col-sm-12 col-xs-12 bsm_title_section">
                <h1 style="color: black">Giới thiệu</h1>
                <p>Cam kết cung cấp cho quý khach dịch vụ tốt nhất, giao hàng nhanh nhất với những đóa hoa chất lượng nhất.</p>
            </div>
        </div>
    </div>
    <div class="bsm_about_us_right_side">
        <img src="public/img/about/bsm_right_side2.png" alt="Giới thiệu">
    </div>
</div>
<?php }
}
